# HalX220

| Spec        |                                                               |
| ----        | ----                                                          |
| Form-factor | Laptop                                                        |
| Manufactor  | Lenovo                                                        |
| Model       | Thinkpad X220                                                 |
| CPU         | Intel Core i5 (2nd Gen) 2520M / 2.5 GHz **(3.2GHz w/ Turbo)** |
| GPU         | Intergrated                                                   |
| Memory      | 16GB                                                          |
| Disks       | 1TB(mSATA SSD) + 512GB (SATA SSD)                             |
