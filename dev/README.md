# Devolper Directory
This directory is meant to be an incubator for current software projects,
being devoloped here until I deem it to be a Minimal Viable Product and spun
off into it's own seperate repo.

## Incubating projects

### Fitness app
An open source fitness reccomendation engine.

### DM-Devices
App for managing table-top roleplaying games

### Permaculture
An open source permaculture reccomendation engine.
