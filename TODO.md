# Todo list for [Unitas](https:/github.com/boldman/unitas) system

### Priority

### Documentation
- [ ] Seperate Nix Ecosystem desciption from home/README to root README
- [ ] Describe use of Home-Manger in jak/README
- [ ] Describe using nix for configuring programs (ie. single file manages extra programs and varibles)
- [ ] Document Hardware
  - [x] HalX220
  - [ ] Iros
  - [ ] Lerna
  - [ ] Helvault
  - [ ] Charon
  - [ ] Aurelia

### Hosts
- [ ] HalX220
  - [ ] Manage itself with NixOps
- [ ] Lerna
  - [ ] Hydra
- [ ] Helvault
  - [ ] NAS
- [ ] Charon
  - [ ] VPN entrypoint *(Wireguard)*
- [ ] Aurelia (VPS)
  - [ ] Wireguard
  - [ ] Email and/or Relay
  - [ ] Website
- [ ] Raspberry Pis
- [ ] Base ISO *(bootstrap NixOps)

### Programs
- [ ] Neovim
  - [ ] Ale
  - [ ] lightline
- [ ] Taskwarrior
  - [ ] timewarrior
  - [ ] bugwarrior
  - [ ] TASKDIR = unitas/.tasks
- [ ] Git
  - [ ] Server *(Gitea)*
  - [ ] git-bug
- [ ] Spaceship

### Services
- [ ] Hydra
- [ ] Email
- [ ] K8s
- [ ] Home-Assistant
- [ ] Matrix Chat
- [ ] Nextcloud
- [ ] MPD
